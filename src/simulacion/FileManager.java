/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulacion;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author Arkabet
 */
public class FileManager {

    public String read(String url) {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        try {
            archivo = new File(url);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);
            String linea;
            String buffer = "";
            while ((linea = br.readLine()) != null) {
                buffer += linea + " ";
            }
            //System.out.println("" + buffer);
            return buffer;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }

        }
    }

    public void write(String url, String sentencia) throws IOException {
        String ruta = url;
        File archivo = new File(ruta);
        BufferedWriter bw;
        if (archivo.exists()) {
            bw = new BufferedWriter(new FileWriter(archivo));
            bw.write(sentencia);
        } else {
            bw = new BufferedWriter(new FileWriter(archivo));
            bw.write(sentencia);
        }
        bw.close();
    }

}
