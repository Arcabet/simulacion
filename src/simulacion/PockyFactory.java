/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulacion;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Arkabet
 */
public class PockyFactory {

    public boolean validacionChi = false;

    public String[] pockyCore(String cadena) {
        String[] out = new String[2];
        boolean valido = false;
        int n = 60, pockyEsperado = 10;
        double chi = 11.07;
        try {
            do {

                int pockys[] = stuffingPockys(cadena, n);

                List<String> pockyPack = stuffingPockyPack(pockys);

                int[] pockyFo = stuffingPockyFo(pockys);

                int[] pockyFe = new int[]{pockyEsperado, pockyEsperado, pockyEsperado, pockyEsperado, pockyEsperado, pockyEsperado};

                out = stuffingPockyBox(pockyFo, pockyFe);

                if (Double.parseDouble(out[1]) <= chi) {
                    System.out.println(chi + " Si es Valido");
                    valido = true;
                    validacionChi = valido;
                } else {
                    System.out.println(chi + " No es Valido");
                    pockyEsperado++;
                    n += 6;
                }
            } while (!valido);
        } catch (Exception e) {
            out[0] = "Contenido erroneo";
            out[1] = "contenido erroneo";
        }
        return out;
    }

    public String[] stuffingPockyBox(int[] pockyFo, int[] pockyFe) {
        double[][] pockyBox = new double[6][5];
        double pockyContainer = 0;
        String buffer[] = new String[2];
        buffer[0] = "";
        System.out.println("clase\tfo\tfe\tfo-fe\t(fo-fe)^2\t(fo-fe)^2/fe");
        buffer[0] += "Clase\tFo\tFe\tFo-Fe\t(Fo-Fe)^2\t(Fo-Fe)^2/fe \n";
        for (int i = 0; i < pockyFo.length; i++) {
            pockyBox[i][0] = Double.parseDouble(("" + pockyFo[i]));
            pockyBox[i][1] = Double.parseDouble(("" + pockyFe[i]));
            pockyBox[i][2] = pockyBox[i][0] - pockyBox[i][1];
            pockyBox[i][3] = Math.pow(pockyBox[i][2], 2);
            pockyBox[i][4] = pockyBox[i][3] / (pockyBox[i][1]);
            pockyContainer += pockyBox[i][4];
            System.out.println((i + 1) + "\t" + pockyBox[i][0] + "\t" + pockyBox[i][1] + "\t" + pockyBox[i][2] + "\t" + pockyBox[i][3] + "\t" + pockyBox[i][4]);
            buffer[0] += "\n" + (i + 1) + "\t" + pockyBox[i][0] + "\t" + pockyBox[i][1] + "\t" + pockyBox[i][2] + "\t" + pockyBox[i][3] + "\t" + pockyBox[i][4] + "\n";
        }
        System.out.print(" Resultado: " + pockyContainer + " < ");
        buffer[1] = "" + pockyContainer;
        return buffer;
    }

    public int[] stuffingPockyFo(int[] pockyPack) {
        int pockyFo[] = new int[]{0, 0, 0, 0, 0, 0};
        for (int i = 0; i < pockyPack.length; i++) {
            switch (pockyPack[i]) {
                case 1:
                    pockyFo[0]++;
                    break;
                case 2:
                    pockyFo[1]++;
                    break;
                case 3:
                    pockyFo[2]++;
                    break;
                case 4:
                    pockyFo[3]++;
                    break;
                case 5:
                    pockyFo[4]++;
                    break;
                case 6:
                    pockyFo[5]++;
                    break;
            }
        }
        return pockyFo;
    }

    public List<String> stuffingPockyPack(int pockys[]) {
        List<String> pockyPack = new ArrayList<String>();
        String buffer = "";
        for (int i = 0, j = 0; i < pockys.length; j++, i++) {
            if (j == 9) {
                j = 0;
                pockyPack.add(buffer);
                buffer = "";
            } else {
                buffer += pockys[i] + " ";

            }
        }
        return pockyPack;
    }

    public int[] stuffingPockys(String cadena, int n) {
        String[] pockys = cadena.split(" ");
        int[] pockyPack = new int[pockys.length];
        for (int i = 0; i < n; i++) {
            pockyPack[i] = (Integer.parseInt(pockys[i]) % 6) + 1;
            System.out.println(i + " Tiro: " + pockyPack[i]);

        }
        return pockyPack;
    }

}
